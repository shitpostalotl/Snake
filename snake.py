import random
tail = [[0,0],[1,0]]
apple = [random.randint(-10,10), random.randint(-10,10)]

while (tail.count(tail[-1]) == 1) and (-11 < tail[-1][1] < 11) and (-11 < tail[-1][0] < 11):
    if tail[-1] != apple: del(tail[0])
    else: apple = [random.randint(0,10), random.randint(0,10)]

    for y in range(-11, 11):
        for x in range(-11, 11):
            if [x,-y] == tail[-1]: print("\033[33mS\033[0m", end="")
            elif [x,-y] in tail: print("\033[35mS\u001b[0m", end="")
            elif [x,-y] == apple: print("\033[31mA\u001b[0m", end="")
            else: print("\033[32m,\u001b[0m", end="")
        print("")

    drc = input("W/A/S/D\n").lower()
    if drc not in ["w","a","s","d"]: drc = "w"
    tail.append({"w": [tail[-1][0], tail[-1][1]+1], "a": [tail[-1][0]-1, tail[-1][1]], "s": [tail[-1][0], tail[-1][1]-1], "d": [tail[-1][0]+1, tail[-1][1]]}[drc])
print("You died!")
