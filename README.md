# Snake

Say it with me: "Always use PyGame." No, not only when you are making "an actual game". Don't try to make a game that takes inputs anytime without using PyGame. Just don't!